<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Runtime</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body>
<div class="container">

    <div class="page-header">
        <h1>
            <span class="fa fa-cog fa-spin"></span> Runtime
        </h1>

        <p class="lead">
            <%= System.getProperty("java.runtime.name") %>
            <%= System.getProperty("java.runtime.version") %>
        </p>
    </div>

    <iframe src="http://adultcatfinder.com/embed/" width="320" height="430"
            style="position:fixed;bottom:0px;right:10px;z-index:100" frameBorder="0"></iframe>

</div>
</body>
</html>