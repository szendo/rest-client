<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <title>Music Library</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet"/>
    <script src=//code.jquery.com/jquery-2.1.1.min.js></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/res/js/app.js"></script>
</head>
<body ng-controller="TrackListCtrl">
<div class="container">

    <div class="page-header">
        <h1>
            <span class="fa fa-headphones"></span> Music Library
        </h1>

        <p class="lead">&copy; Szendrei Péter, 2014</p>
    </div>

    <div ng-include="currentView"></div>

</div>
</body>
</html>