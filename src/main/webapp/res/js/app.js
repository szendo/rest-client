(function () {
    "use strict";

    var app = angular.module('app', []);

    app.controller('TrackListCtrl', ['$scope', '$filter', '$http', function ($scope, $filter, $http) {

        $scope.currentView = 'albums.html';

        $scope.navigateTo = function (viewName) {
            $scope.currentView = viewName + '.html';
        };

        $scope.$watch('getAlbumsCurrUrl', function (newValue) {
            if (newValue) {
                $scope.refreshAlbums()
            }
        });

        $scope.$watch('getTracksUrl', function (newValue) {
            $scope.tracks = [];
            if (newValue) {
                $scope.refreshTracks();
            }
        });

        $scope.changeGetAlbumsUrl = function (newUrl) {
            if (newUrl !== null) {
                $scope.getAlbumsCurrUrl = newUrl;
            }
        };

        $scope.refreshAlbums = function () {
            $scope.getAlbumsRefreshing = true;
            $http.get($scope.getAlbumsCurrUrl)
                .success(function (data) {
                    $scope.albums = data.content;
                    $scope.getAlbumsNextUrl = null;
                    $scope.getAlbumsPrevUrl = null;

                    var prevLink = $filter('filter')(data.links, {rel: 'prev'})[0];
                    if (prevLink) $scope.getAlbumsPrevUrl = prevLink.href;

                    var nextLink = $filter('filter')(data.links, {rel: 'next'})[0];
                    if (nextLink) $scope.getAlbumsNextUrl = nextLink.href;

                    $scope.getAlbumsRefreshing = false;
                })
                .error(function () {
                    $scope.getAlbumsRefreshing = false;
                });
        };

        $scope.refreshTracks = function () {
            $scope.getTracksRefreshing = true;
            $http.get($scope.getTracksUrl)
                .success(function (data) {
                    $scope.tracks = data;
                    $scope.getTracksRefreshing = false;
                })
                .error(function () {
                    $scope.getTracksRefreshing = false;
                });
        };

        $scope.clearNewAlbum = function () {
            $scope.newAlbum = {
                name: '',
                artist: ''
            };
            $scope.newAlbumError = false;
        };

        $scope.saveNewAlbum = function () {
            $scope.newAlbumError = false;
            $http.post('/rest/albums', $scope.newAlbum)
                .success(function (data, status, headers) {
                    $scope.refreshAlbums();
                    $('#newAlbumModal').modal('hide');
                    $http.get(headers('Location'))
                        .success(function (data) {
                            $scope.navigateToAlbum(data);
                        });
                })
                .error(function () {
                    $scope.newAlbumError = true;
                });
        };

        $scope.navigateToAlbum = function (album) {
            $scope.loadEditAlbum(album);
            $scope.navigateTo('album');
        };

        $scope.loadEditAlbum = function (album) {
            $scope.editAlbum = {
                name: album.name,
                artist: album.artist
            };
            $scope.editAlbumError = false;

            var albumUrl = $filter('filter')(album.links, {rel: 'self'})[0];
            if (albumUrl) $scope.editAlbumUrl = albumUrl.href;

            var albumTracksUrl = $filter('filter')(album.links, {rel: 'tracks'})[0];
            if (albumTracksUrl) $scope.getTracksUrl = albumTracksUrl.href;
        };

        $scope.saveEditAlbum = function () {
            $http.put($scope.editAlbumUrl, $scope.editAlbum)
                .success(function (data) {
                    $scope.loadEditAlbum(data);
                    $scope.editAlbumError = false;
                    $scope.refreshAlbums();
                })
                .error(function () {
                    $scope.editAlbumError = true;
                });
        };

        $scope.deleteEditAlbum = function () {
            $http.delete($scope.editAlbumUrl)
                .success(function () {
                    $scope.refreshAlbums();
                    $scope.navigateTo('albums');
                })
                .error(function () {
                });
        };

        $scope.clearNewTrack = function () {
            $scope.newTrack = {
                name: ''
            };
            $scope.newTrackError = false;
        };

        $scope.saveNewTrack = function () {
            $scope.newTrackError = false;
            $http.post($scope.getTracksUrl, $scope.newTrack)
                .success(function () {
                    $scope.refreshTracks();
                    $('#newTrackModal').modal('hide');
                })
                .error(function () {
                    $scope.newTrackError = true;
                });
        };

        $scope.navigateToTrack = function (track) {
            $scope.loadEditTrack(track);
            $scope.navigateTo('track');
        };

        $scope.loadEditTrack = function (track) {
            $scope.editTrack = {
                name: track.name
            };
            $scope.editTrackError = false;

            var trackUrl = $filter('filter')(track.links, {rel: 'self'})[0];
            if (trackUrl) $scope.editTrackUrl = trackUrl.href;
        };

        $scope.saveEditTrack = function () {
            $http.put($scope.editTrackUrl, $scope.editTrack)
                .success(function (data) {
                    $scope.loadEditTrack(data);
                    $scope.editTrackError = false;
                    $scope.refreshTracks();
                })
                .error(function () {
                    $scope.editTrackError = true;
                });
        };

        $scope.deleteEditTrack = function () {
            $http.delete($scope.editTrackUrl)
                .success(function () {
                    $scope.refreshTracks();
                    $scope.navigateTo('album');
                })
                .error(function () {
                });
        };

        $scope.log = function () {
            console.log.apply(console, arguments);
        };

        $scope.setActive = function (target, val) {
            if (val) {
                $(target).addClass('active');
            } else {
                $(target).removeClass('active');
            }
        };

        $scope.getAlbumsCurrUrl = '/rest/albums';
        $scope.getAlbumCurrUrl = null;

    }]);

}());